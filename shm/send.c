#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

#define SHM_SIZE 512

int main() {
	char buf[1024];
	int shm = shmget(IPC_PRIVATE, SHM_SIZE, IPC_CREAT | 0666);
	if (shm == -1) {
		perror("共有メモリの作成");
		exit(EXIT_FAILURE);
	}
	printf("共有メモリのID: %d\n", shm);
	
	char *addr = shmat(shm, NULL, 0);
	if (addr == (char *)-1) {
		perror("共有メモリのアタッチ");
		exit(EXIT_FAILURE);
	}

	snprintf(addr, SHM_SIZE, "Hello, world!");

	printf("qを入力したら終了します\n");
	while (1) {
		fgets(buf, 1024, stdin);
		if (strncmp(buf, "q", 1) == 0) {
			break;
		}
	}

	if (shmdt(addr) == -1) {
		perror("共有メモリの分離");
		exit(EXIT_FAILURE);
	}
	if (shmctl(shm, IPC_RMID, NULL) == -1) {
		perror("共有メモリの削除");
		exit(EXIT_FAILURE);
	}
	return 0;
}
