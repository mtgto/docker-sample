#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>

int main(int argc, char **argv) {
	if (argc <= 1) {
		fprintf(stderr, "Usage: %s <共有メモリのID>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int shm = atoi(argv[1]);

	char *addr = shmat(shm, NULL, 0);
	if (addr == (char *)-1) {
		perror("共有メモリのアタッチ");
		exit(EXIT_FAILURE);
	}

	printf("共有メモリの内容: %s\n", addr);

	if (shmdt(addr) == -1) {
		perror("共有メモリの分離");
		exit(EXIT_FAILURE);
	}
	return 0;
}
